import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Actions, Effect, ofType } from '@ngrx/effects'; //ACTIONS one big observable that will give access to all dispatched actions so that we can react to them.
import { catchError, map, of, switchMap, tap } from 'rxjs';
import { AuthService } from '../auth.service';
import { User } from '../user.model';

import * as AuthActions from './auth.actions';

export interface AuthResponseData {
  kind: string;
  idToken: string;
  email: string;
  refreshToken: string;
  expiresIn: string;
  localId: string;
  registered?: boolean;
}

const handleAuthentication = (
  expiresIn: number,
  email: string,
  userId: string,
  token: string
) => {
  const expirationDate = new Date(new Date().getTime() + expiresIn * 10000);
  const user = new User(email, userId, token, expirationDate);
  localStorage.setItem('userData', JSON.stringify(user));
  return new AuthActions.AuthenticateSuccess({
    email: email,
    userId: userId,
    token: token,
    expirationDate: expirationDate,
    redirect: true
  });
};

const handleError = (errorRes: any) => {
  let errorMessage = 'An unknown error occured!';
  if (!errorRes.error || !errorRes.error.error) {
    return of(new AuthActions.AuthenticateFail(errorMessage));
  }
  switch (errorRes.error.error.message) {
    case 'EMAIL_EXISTS':
      errorMessage = 'This email exists already';
      break;
    case 'EMAIL_NOT_FOUND':
      errorMessage = 'This email doesnot exist';
      break;
    case 'INVALID_PASSWORD':
      errorMessage = 'This password is not correct';
      break;
  }
  return of(new AuthActions.AuthenticateFail(errorMessage));
};

@Injectable()
export class AuthEffects {
  @Effect()
  authSignup = this.actions$.pipe(
    ofType(AuthActions.SIGNUP_START),
    switchMap((signupAction: AuthActions.SignupStart) => {
      return this.http
        .post<AuthResponseData>(
          'https://identitytoolkit.googleapis.com/v1/accounts:signUp?key=AIzaSyDFYKTSSBWuzEKFTMlh2D0u8Jj_6zDyq7A',
          {
            email: signupAction.payload.email,
            password: signupAction.payload.password,
            returnSecureToken: true,
          }
        )
        .pipe(
          tap((resData) => {
            this.authService.setLogoutTimer(+resData.expiresIn * 1000);
          }),
          map((resData) => {
            // const expirationDate = new Date(
            //   new Date().getTime() + +resData.expiresIn * 10000
            // );
            // return new AuthActions.AuthenticateSuccess({
            //   email: resData.email,
            //   userId: resData.localId,
            //   token: resData.idToken,
            //   expirationDate: expirationDate,
            // });
            return handleAuthentication(
              +resData.expiresIn,
              resData.email,
              resData.localId,
              resData.idToken
            );
          }),
          catchError((errorRes) => {
            // let errorMessage = 'An unknown error occured!';
            // if (!errorRes.error || !errorRes.error.error) {
            //   return of(new AuthActions.AuthenticateFail(errorMessage));
            // }
            // switch (errorRes.error.error.message) {
            //   case 'EMAIL_EXISTS':
            //     errorMessage = 'This email exists already';
            //     break;
            //   case 'EMAIL_NOT_FOUND':
            //     errorMessage = 'This email doesnot exist';
            //     break;
            //   case 'INVALID_PASSWORD':
            //     errorMessage = 'This password is not correct';
            //     break;
            // }
            // return of(new AuthActions.AuthenticateFail(errorMessage));
            return handleError(errorRes);
          })
        );
    })
  );

  @Effect()
  authLogin = this.actions$.pipe(
    ofType(AuthActions.LOGIN_START),
    switchMap((authData: AuthActions.LoginStart) => {
      return this.http
        .post<AuthResponseData>(
          'https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=AIzaSyDFYKTSSBWuzEKFTMlh2D0u8Jj_6zDyq7A',
          {
            email: authData.payload.email,
            password: authData.payload.password,
            returnSecureToken: true,
          }
        )
        .pipe(
          tap((resData) => {
            this.authService.setLogoutTimer(+resData.expiresIn * 1000);
          }),
          map((resData) => {
            // const expirationDate = new Date(
            //   new Date().getTime() + +resData.expiresIn * 10000
            // );
            // return new AuthActions.AuthenticateSuccess({
            //   email: resData.email,
            //   userId: resData.localId,
            //   token: resData.idToken,
            //   expirationDate: expirationDate,
            // });
            return handleAuthentication(
              +resData.expiresIn,
              resData.email,
              resData.localId,
              resData.idToken
            );
          }),
          catchError((errorRes) => {
            // let errorMessage = 'An unknown error occured!';
            // if (!errorRes.error || !errorRes.error.error) {
            //   return of(new AuthActions.AuthenticateFail(errorMessage));
            // }
            // switch (errorRes.error.error.message) {
            //   case 'EMAIL_EXISTS':
            //     errorMessage = 'This email exists already';
            //     break;
            //   case 'EMAIL_NOT_FOUND':
            //     errorMessage = 'This email doesnot exist';
            //     break;
            //   case 'INVALID_PASSWORD':
            //     errorMessage = 'This password is not correct';
            //     break;
            // }
            // return of(new AuthActions.AuthenticateFail(errorMessage));
            return handleError(errorRes);
          })
        );
    })
  );

  //   @Effect({ dispatch: false })
  //   authSuccess = this.actions$.pipe(
  //     ofType(AuthActions.AUTHENTICATE_SUCCESS),
  //     tap(() => {
  //       this.router.navigate(['/']);
  //     })
  //   );

  @Effect({ dispatch: false })
  authRedirect = this.actions$.pipe(
    // ofType(AuthActions.AUTHENTICATE_SUCCESS, AuthActions.LOGOUT),
    ofType(AuthActions.AUTHENTICATE_SUCCESS),
    tap((authSuccessAction: AuthActions.AuthenticateSuccess) => {
      if(authSuccessAction.payload.redirect){
        this.router.navigate(['/']);
      }
    })
  );
  @Effect({ dispatch: false })
  authLogout = this.actions$.pipe(
    ofType(AuthActions.LOGOUT),
    tap(() => {
      this.authService.clearLogoutTimer();
      localStorage.removeItem('userData');
      this.router.navigate(['/auth']);
    })
  );

  @Effect()
  autologin = this.actions$.pipe(
    ofType(AuthActions.AUTO_LOGIN),
    map(() => {
      const userData: {
        email: string;
        id: string;
        _token: string;
        _tokenExpirationDate: string;
      } = JSON.parse(localStorage.getItem('userData'));
      if (!userData) {
        return { type: 'DUMMY' };
      }
      const loadedUser = new User(
        userData.email,
        userData.id,
        userData._token,
        new Date(userData._tokenExpirationDate)
      );

      if (loadedUser.token) {
        // this.user.next(loadedUser);
        const expirationDuration =
          new Date(userData._tokenExpirationDate).getTime() -
          new Date().getTime();
        this.authService.setLogoutTimer(expirationDuration);
        return new AuthActions.AuthenticateSuccess({
          email: loadedUser.email,
          userId: loadedUser.email,
          token: loadedUser.token,
          expirationDate: new Date(userData._tokenExpirationDate),
          redirect: false
        });
        // const expirationDuration =
        //   new Date(userData._tokenExpirationDate).getTime() -
        //   new Date().getTime();
        // this.autoLogout(expirationDuration);
      }
      return { type: 'DUMMY' };
    })
  );
  constructor(
    private actions$: Actions,
    private http: HttpClient,
    private router: Router,
    private authService: AuthService
  ) {}
}
