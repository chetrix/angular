import { Recipe } from './recipe.model';
import { EventEmitter, Injectable } from '@angular/core';
import { Ingredient } from '../shared/ingredient.model';
// import { ShoppingListService } from '../shopping-list/shopping-list.service';
import { Subject } from 'rxjs';
import { Store } from '@ngrx/store';
import * as ShoppingListActions from '../shopping-list/store/shopping-list.actions';
import * as fromShoppingList from '../shopping-list/store/shopping-list.reducer';
import * as fromApp from '../store/app.reducer';
@Injectable()
export class RecipeService {
  // recipeSelected = new EventEmitter<Recipe>();
  // recipeSelected = new Subject<Recipe>();
  recipesChanged = new Subject<Recipe[]>();
  // private recipes: Recipe[] = [
  //   new Recipe(
  //     'A test Recipe',
  //     'This is simple test',
  //     'https://imagesvc.meredithcorp.io/v3/mm/image?url=https%3A%2F%2Fpublic-assets.meredithcorp.io%2Fa3fb2fb40f7208d148aed9ec2de15c8c%2F1645499384image.jpg&w=595&h=398&c=sc&poi=face&q=60',
  //     [
  //       new Ingredient('Meat',1),
  //       new Ingredient('French fries',20)
  //     ]
  //   ),
  //   new Recipe(
  //     'Another recipe',
  //     'This is another test recipe',
  //     'https://imagesvc.meredithcorp.io/v3/mm/image?url=https%3A%2F%2Fpublic-assets.meredithcorp.io%2Fa3fb2fb40f7208d148aed9ec2de15c8c%2F1645499384image.jpg&w=595&h=398&c=sc&poi=face&q=60',
  //     [
  //       new Ingredient('Meat',1),
  //       new Ingredient('French fries',20)
  //     ]
  //   ),
  // ];

  private recipes: Recipe[] = [];
  constructor(
    // private slService: ShoppingListService,
    // private store: Store<fromShoppingList.AppState>
    private store: Store<fromApp.AppState>

  ) {}
  getRecipes() {
    return this.recipes.slice(); //we are using slice to avoid reference issue cause if we return without slice and if change the value of the recipe from other component it changes the main recipes as well. deep copy and shallow copy concept.
  }

  addIngredientsToShoppingList(ingredient: Ingredient[]) {
    // this.slService.addIngredients(ingredient)
    this.store.dispatch(new ShoppingListActions.AddIngredients(ingredient));
  }

  getRecipe(index: number) {
    return this.recipes[index];
  }

  addRecipe(recipe: Recipe) {
    this.recipes.push(recipe);
    this.recipesChanged.next(this.recipes.slice());
  }

  updateRecipe(index: number, newRecipe: Recipe) {
    this.recipes[index] = newRecipe;
    this.recipesChanged.next(this.recipes.slice());
  }
  deleteRecipe(index: number) {
    this.recipes.splice(index, 1);
    this.recipesChanged.next(this.recipes.slice());
  }

  setRecipes(recipes: Recipe[]) {
    this.recipes = recipes;
    this.recipesChanged.next(this.recipes.slice());
  }
}
