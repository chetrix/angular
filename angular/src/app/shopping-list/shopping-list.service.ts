// // import { EventEmitter } from '@angular/core';
// import { Ingredient } from '../shared/ingredient.model';
// import { Subject } from 'rxjs';

// export class ShoppingListService {
//   // ingridentsChanged = new EventEmitter<Ingredient[]>();
//   ingridentsChanged = new Subject<Ingredient[]>();
//   startedEditing = new Subject<number>();

//   private ingredients: Ingredient[] = [
//     new Ingredient('Apples', 5),
//     new Ingredient('Tomatoes', 10),
//   ];

//   getIngredients() {
//     return this.ingredients.slice();
//   }

//   getIngredient(index: number){
//     return this.ingredients[index];
//   }

//   addIngredint(ingredient: Ingredient) {
//     this.ingredients.push(ingredient);
//     // this.ingridentsChanged.emit(this.ingredients.slice());
//     this.ingridentsChanged.next(this.ingredients.slice());
//   }

//   addIngredients(ingredients: Ingredient[]) {
//     //       for(let ingredient of ingredients){
//     //           this.addIngredint(ingredient);
//     //       }
//     this.ingredients.push(...ingredients);
//     // this.ingridentsChanged.emit(this.ingredients.slice());
//     this.ingridentsChanged.next(this.ingredients.slice());
//   }

//   updateIngredient(index: number, newIngredient: Ingredient){
//     this.ingredients[index] = newIngredient;
//     this.ingridentsChanged.next(this.ingredients.slice());
//   }

//   deleteIngredient(index: number){
//     this.ingredients.splice(index,1);
//     this.ingridentsChanged.next(this.ingredients.slice());
//   }
// }
