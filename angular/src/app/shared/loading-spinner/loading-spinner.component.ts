import { Component } from "@angular/core";

@Component({
    selector: 'app-loading-spinner',
    template: '<div class="loadingio-spinner-rolling-dcsgde7kpu7"><div class="ldio-dho9pttthq"><div></div></div></div>',
    styleUrls: ['./loading-spinner.component.css']
})
export class LoadingSpinnerComponent{

}